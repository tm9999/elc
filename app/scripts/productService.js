const URL = 'http://localhost:3035/products';

const searchProducts = (search) => {
	return fetch(URL+`?search=${search}`).then(res => res.json()).then(data => {return data;})

}

export { searchProducts }