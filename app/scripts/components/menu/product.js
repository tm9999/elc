import React from 'react'

const Product = (props) => {
    return (
        <div className="product">
            <div className="img-container">
                <img src={props.picture} alt="product image"/>
            </div>
            <div>
                <p className="name">{props.name}</p>
                <p className="about" title={props.about}>{props.about}</p>
            </div>

        </div>
    )
}

export default Product