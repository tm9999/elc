import React from 'react'
import Product from './product.js'

const SearchResults = (props) => {
    console.log(props)
    return (
        <div className="search-results-container">
            <div className="header">
                <span>SHOWING {props.products.length} of {props.total} RESULTS</span>
                <a href="#" className="show-all">SHOW ALL RESULTS</a>
            </div>
            <div className="results">
                {props.products.map(p => <Product key={p._id} {...p} />)}
            </div>
        </div>
    )
}

export default SearchResults