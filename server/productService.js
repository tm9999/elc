const data      = require('./data');
const NUM_RESULTS_TO_RETURN = 4;

const allProducts = () => {
	return data
}

/* returns products that match keyword
** (Note - in a real world scenario this would be able to handle fuzzy search and filter criteria)
*/
const productSearch = (keyword) => {
	const search = keyword.toLowerCase();
	const results = data.filter(d => {
		const searchString = `${d.name} ${d.tags.join(' ')}`.toLowerCase();
		return searchString.indexOf(keyword) > -1
	});

	const payload = {
		products: results.slice(0, NUM_RESULTS_TO_RETURN),
		total: results.length
	}

	return payload ;
}

module.exports = { allProducts, productSearch }
